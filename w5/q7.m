A = imread('noisy.jpg');
dA = im2double(A);

F1 = medfilt2(dA);
F2 = medfilt2(F1);

orig = imread('original.jpg');
dorig = im2double(orig);

value = psnr(dA, dorig, 1)

value1 = psnr(F2, dorig, 1)

value2 = psnr(F1, dorig, 1)