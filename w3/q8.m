originalRGB = imread('original_quiz.jpg');
dRGB = im2double(originalRGB);
myfilter = ones(3,3).*(1/9);
filteredRGB = imfilter(dRGB, myfilter, 'replicate');

%step(3)
%down-sampling the image by removing every other row and colum from the
%filtered image
filteredRGBdown = filteredRGB(1:2:359,1:2:479);

%step(4)
%create all-zeros Matlab array of width 479 and height 359
over = zeros(359,479);
for i = 1:2:359
    for j = 1:2:479
        over(i,j) = filteredRGBdown((i+1)/2,(j+1)/2);
    end
end

%step(5)
%perform bilinear interpolation to obtain up-sampled image
ufilter = [0.25 0.5 0.25;0.5 1 0.5;0.25 0.5 0.25];
upsampledRGB = imfilter(over,ufilter);

%step(6) compute PSNR
value = psnr(upsampledRGB, dRGB, 1)