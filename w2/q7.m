originalRGB = imread('Flena.gif');
myfilter = ones(3,3).*(1/9);
dRGB = im2double(originalRGB);
filteredRGB = imfilter(dRGB, myfilter, 'replicate');
figure, imshow(filteredRGB)

value = psnr(filteredRGB, dRGB, 1)

myfilter = ones(5,5).*(1/25);
filteredRGB = imfilter(dRGB, myfilter, 'replicate');

value = psnr(filteredRGB, dRGB, 1)